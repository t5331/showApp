FROM node:14-slim
WORKDIR ./
COPY package.json .
COPY package-lock.json .
RUN npm cache clean --force
RUN npm install
COPY . .


ENV DB_HOST=${DB_HOST}
ENV DB_USER=${DB_USER}
ENV DB_NAME_DEVELOPMENT=${DB_NAME_DEVELOPMENT}
ENV DB_NAME_PRODUCTION=${DB_NAME_PRODUCTION}
ENV DB_NAME_TEST=${DB_NAME_TEST}
ENV DB_PORT=${DB_PORT}
ENV DB_PASS=${DB_PASS}
ENV APP_ENV=${APP_ENV}
ENV APP_NAME=${APP_NAME}
ENV APP_URL=${APP_URL}
ENV APP_PORT=${APP_PORT}
ENV SECRET_KEY=${SECRET_KEY}


EXPOSE ${APP_PORT}

CMD echo "DB_HOST=${DB_HOST}" > .env \
    &&  echo "DB_USER=${DB_USER}" >> .env \
    &&  echo "DB_NAME_DEVELOPMENT=${DB_NAME_DEVELOPMENT}" >> .env \
    &&  echo "DB_NAME_PRODUCTION=${DB_NAME_PRODUCTION}" >> .env \
    &&  echo "DB_NAME_TEST=${DB_NAME_TEST}" >> .env \
    &&  echo "DB_HOST=${DB_HOST}" >> .env \
    &&  echo "DB_PORT=${DB_PORT}" >> .env \
    &&  echo "DB_PASS=${DB_PASS}" >> .env \
    &&  echo "APP_ENV=${APP_ENV}" >> .env \
    &&  echo "APP_NAME=${APP_NAME}" >> .env \
    &&  echo "APP_URL=${APP_URL}" >> .env \
    &&  echo "APP_PORT=${APP_PORT}" >> .env \
    &&  echo "SECRET_KEY=${SECRET_KEY}" >> .env \
    &&  npm run start:dev
