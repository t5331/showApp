import type { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { DatabaseConfigService } from './src/common/config/database/config.service';
import { User } from './src/users/entities/user.entity';
import { Ticket } from './src/tickets/entities/ticket.entity';
import { Organization } from './src/organizations/entities/organization.entity';
import { Request } from './src/requests/entities/request.entity';
import { Event } from './src/events/entities/event.entity';

const DatabaseConfig: TypeOrmModuleOptions & {
  seeds: string[];
} = {
  type: 'postgres',
  host: DatabaseConfigService.host,
  port: DatabaseConfigService.port,
  username: DatabaseConfigService.username,
  password: DatabaseConfigService.password,
  database: DatabaseConfigService.database,
  entities: [User, Organization, Event, Ticket, Request],
  //   migrations: [join(__dirname, '../../..') + '/migrations/*.ts'],
  seeds: ['src/seeds/**/*{.ts,.js}'],
  // factories: [join(__dirname, '../../..') + '/seeds/factory/*.factory.ts'],
  synchronize: process.env.APP_ENV !== 'production',
};

module.exports = DatabaseConfig;
