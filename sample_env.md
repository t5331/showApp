APP_ENV=development
APP_NAME="ShowApp"
APP_URL=http://localhost:3000
APP_PORT=3000

DB_HOST=
DB_PORT =
DB_USER=
DB_PASS=
DB_NAME_DEVELOPMENT=showappdb_development
DB_NAME_TEST=showappdb_test
DB_NAME_PRODUCTION=showappdb_production
