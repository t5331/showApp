import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { TicketsService } from './tickets.service';
import { CreateTicketDto } from './dto/create-ticket.dto';
import {
  ApiBearerAuth,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { GetUser } from 'src/auth/get-user.decorator';
import { User } from 'src/users/entities/user.entity';
import { AuthGuard } from '@nestjs/passport';
import { UserRole } from 'src/common/enums/user-role.enum';
import { Roles } from 'src/auth/decorators/roles.decorator';

@ApiTags('Ticket')
@Controller('tickets')
export class TicketsController {
  constructor(private ticketsService: TicketsService) {}

  @ApiTags('User journey')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @Post()
  @ApiResponse({ status: 200, description: 'Ticket created successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  async createTicket(@Body() body: CreateTicketDto, @GetUser() user: User) {
    return await this.ticketsService.create(body, user);
  }

  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @Get('/')
  @ApiResponse({ status: 200, description: 'All tickets created successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  getAllTickets(@Query('page') page, @Query('size') size) {
    return this.ticketsService.getAll(page, size);
  }

  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @Get('/user')
  @ApiResponse({
    status: 200,
    description: 'All your tickets created successfully',
  })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  getAllTicketsByUser(
    @GetUser() user: User,
    @Query('page') page,
    @Query('size') size,
  ) {
    return this.ticketsService.getAllByUser(user, page, size);
  }

  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @Get('/:id')
  @ApiResponse({ status: 200, description: 'Ticket retrieved successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  getATicket(@Param('id') id: string) {
    return this.ticketsService.getOne(id);
  }

  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @Patch('/refund/:id')
  @ApiResponse({ status: 200, description: 'Ticket retrieved successfully' })
  @ApiNotFoundResponse({ description: 'This ticket does not exist' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  approveTicketRefund(@Param('id') id: string) {
    return this.ticketsService.approveRefund(id);
  }
}
