import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UseFilters,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateTicketDto } from './dto/create-ticket.dto';
import { toDataURL } from 'qrcode';
import { Event } from 'src/events/entities/event.entity';
import { Ticket } from './entities/ticket.entity';
import { TicketRepository } from './ticket.repository';
import { HttpExceptionFilter } from 'src/common/exceptions/http-exception.filter';
import { successResponse } from 'src/common/helpers/responses/success.helper';
import { EventsService } from 'src/events/events.service';
import { omit } from 'lodash';
import { ticketEmailTemplate } from 'src/common/helpers/ticket-email-template';
import { SendgridService } from 'src/common/utils/sendgrid.service';
import { EventStatus } from 'src/common/enums/event-status.enum';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class TicketsService {
  constructor(
    @InjectRepository(Ticket) private ticketRepo: TicketRepository,
    private eventRepo: EventsService,
    private userService: UsersService,
  ) {}

  public async create(ticketToCreate: CreateTicketDto, user: any) {
    const qrCodeData = JSON.stringify({
      user: {
        ...omit(user, ['password', 'activated']),
      },
      ...ticketToCreate,
    });
    const getEvent = await this.findEvent(ticketToCreate.eventId);
    if (getEvent.status != EventStatus.ACTIVE)
      throw new BadRequestException('Event already ended');
    // eslint-disable-next-line prefer-const
    const totalprice =
      getEvent[ticketToCreate.seatCategory].price *
      ticketToCreate.ticketQuantity;
    const arr = [];
    await Promise.all(
      [...Array(ticketToCreate.ticketQuantity)].map(async (i) => {
        const result = await this.ticketRepo.save({
          seatCategory: ticketToCreate.seatCategory,
          price: `${totalprice}`,
          seatNumber: 1,
          user: user.id,
          event: ticketToCreate.eventId,
        });
        await this.generateQrCode(qrCodeData, async (code: string) => {
          new SendgridService().send(this.ticketMail(user, code));
        });
        arr.push(result);
      }),
    );
    const data = {
      [ticketToCreate.seatCategory]: {
        ...getEvent[ticketToCreate.seatCategory],
        availableSeats:
          getEvent[ticketToCreate.seatCategory].availableSeats -
          ticketToCreate.ticketQuantity,
      },
    };
    await this.updateEvent(ticketToCreate.eventId, data);
    return successResponse('Ticket has been created successfully', arr);
  }

  private async findEvent(id: string): Promise<Event> {
    return await this.eventRepo.findOne(id);
  }

  private async updateEvent(id: string, data: any): Promise<Event> {
    return await this.eventRepo.updateEvent(id, data);
  }

  public async getAll(page: number, size: number) {
    const data = await this.ticketRepo.findAndCount({
      order: {
        createdAt: 'DESC',
      },
      skip: (page - 1) * size,
      take: size || 10,
      relations: ['user', 'event'],
    });
    return successResponse('All tickets returned successfully', data);
  }

  public async getAllByUser(user: any, page: number, size: number) {
    const data = await this.ticketRepo.findAndCount({
      where: { userId: user.id },
      order: {
        createdAt: 'DESC',
      },
      skip: (page - 1) * size,
      take: size || 10,
      relations: ['user', 'event'],
    });
    return successResponse('All your tickets returned successfully', data);
  }

  public async getOne(id: string) {
    const result = await this.ticketRepo.findOne({
      where: {
        id,
      },
      relations: ['user', 'event'],
    });
    return successResponse('Ticket returned successfully', result);
  }

  @UseFilters(new HttpExceptionFilter())
  public async approveRefund(id: string) {
    await this.findTicket(id);
    const result = await this.ticketRepo.update(id, { refunded: true });
    return successResponse('Ticket refunded successfully', result);
  }

  private async findTicket(condition: any) {
    const ticket = await this.ticketRepo.findOne(condition);
    if (!ticket) {
      throw new NotFoundException('This ticket does not exist');
    } else {
      return ticket;
    }
  }

  private async generateQrCode(stringdata: any, callback: any) {
    toDataURL(stringdata, async function (err, code) {
      if (err) throw new BadRequestException(err);
      callback(code);
    });
  }

  private ticketMail(user: any, code: string) {
    return {
      to: user.email,
      subject: 'ShowApp ticket qrCode',
      from: '',
      text: `Hello ${user.names}, here's your ticket qrcode you created`,
      html: ticketEmailTemplate(code),
    };
  }
}
