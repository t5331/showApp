import { IsNotEmpty } from 'class-validator';
import { seatCategories } from '../entities/ticket.entity';

export class CreateTicketDto {
  @IsNotEmpty()
  seatCategory: seatCategories;
  @IsNotEmpty()
  eventId: any;
  @IsNotEmpty()
  ticketQuantity: number;
}
