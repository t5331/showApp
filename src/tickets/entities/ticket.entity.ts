import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { Event } from '../../events/entities/event.entity';

export enum seatCategories {
  ORDINALY = 'ordinaly',
  VIP = 'vip',
  VVIP = 'vvip',
  TABLE = 'table',
}

@Entity()
export class Ticket {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({
    type: 'enum',
    enum: seatCategories,
    default: seatCategories.ORDINALY,
  })
  seatCategory: seatCategories;
  @Column({ nullable: false })
  price: string;
  @Column({ nullable: false })
  seatNumber: number;
  @Column({ nullable: false, default: false })
  refunded: boolean;
  @CreateDateColumn({
    type: 'timestamptz',
    default: 'NOW()',
  })
  createdAt: Date;
  @UpdateDateColumn({
    type: 'timestamptz',
    onUpdate: 'NOW()',
  })
  updatedAt: Date;

  @ManyToOne(() => Event)
  event: Event;
  @ManyToOne(() => User, (user) => user.tickets)
  user: User;
}
