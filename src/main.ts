import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule } from '@nestjs/swagger';
import { AppConfigService } from './common/config/app/config.service';
import { NestExpressApplication } from '@nestjs/platform-express';
import { config, customOptions } from './common/config/swagger-config';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const appConfig: AppConfigService = app.get(AppConfigService);
  app.setGlobalPrefix('api/v1');
  const document = SwaggerModule.createDocument(app, config,);
  
  SwaggerModule.setup('api/v1/documentation', app, document, customOptions);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );
  await app.listen(appConfig.port);
}
bootstrap();
