import { Exclude } from 'class-transformer';

export class UserSerializer {
  id: string;
  names: string;
  email: string;
  phone: string;

  @Exclude()
  password: string;
}
