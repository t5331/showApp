import { Organization } from '../../organizations/entities/organization.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import { Ticket } from '../../tickets/entities/ticket.entity';
import { UserRole } from '../../common/enums/user-role.enum';
import { Request } from '../../requests/entities/request.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ nullable: false })
  names: string;
  @Column({ nullable: false, unique: true })
  email: string;
  @Column({ nullable: false })
  phone: string;
  @Column({ nullable: false })
  password: string;
  @Column({ nullable: false, default: false })
  activated: boolean;
  @CreateDateColumn({
    type: 'timestamptz',
    default: 'NOW()',
  })
  createdAt: Date;
  @UpdateDateColumn({
    type: 'timestamptz',
    onUpdate: 'NOW()',
  })
  updatedAt: Date;

  @Column({
    nullable: true,
  })
  verificationCode: number;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.ATTENDEE,
  })
  role: UserRole;

  @OneToMany(() => Ticket, (ticket) => ticket.user)
  tickets: Ticket[];

  @OneToMany(() => Request, (request) => request.user)
  requests: Request[];
  @OneToMany(() => Organization, (organizations) => organizations.user)
  organizations: Organization[];
}
