import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsPhoneNumber,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';

export class CreateUserDto {
  @ApiProperty({
    description: 'Username',
    default: 'CYIMANA Faisal',
    type: 'string',
  })
  @IsNotEmpty()
  names: string;
  @ApiProperty({
    description: 'Email',
    default: 'faisalcyimana1998@gmail.com',
    type: 'string',
  })
  @IsNotEmpty()
  @IsEmail()
  email: string;
  @ApiProperty({
    description: 'Phone number',
    default: '+250780508308',
    type: 'string',
  })
  @IsNotEmpty()
  @IsPhoneNumber()
  phone: string;
  @ApiProperty({
    description: 'Password',
    default: '@pass123',
    type: 'string',
  })
  @IsNotEmpty()
  @MinLength(6)
  @MaxLength(32)
  @Matches(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/, {
    message: 'Password must have at least a number, a special character',
  })
  password: string;
}
