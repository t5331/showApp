import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRole } from '../common/enums/user-role.enum';

import { UserRepository } from './users.repository';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {}

  async findOne(id: string) {
    return await this.userRepository.findOne(id);
  }
  async makeUserOrganizer(id: string) {
    const user = await this.findOne(id);
    if (!user) throw new NotFoundException('user not found');
    return this.userRepository.save({ ...user, role: UserRole.ORGANIZER });
  }
}
