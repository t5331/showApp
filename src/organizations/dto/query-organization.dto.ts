import { IsNotEmpty, IsString, Matches } from 'class-validator';

export class QueryOrganizationDto {
  @IsString()
  @IsNotEmpty()
  @Matches(/.?[^!@#$%*&^]/, { message: 'Invalid search keyword' })
  key: string;
}
