import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
  Length,
  Matches,
} from 'class-validator';
import { User } from '../../users/entities/user.entity';

export class CreateOrganizationDto {
  @IsString()
  @IsNotEmpty()
  @Matches(/.?[^!@#$%*&^]/, {
    message: 'Organization name should have at least a Letter',
  })
  @ApiProperty({
    description: 'Organization Name',
    default: 'The rundown fashion show',
    type: String,
  })
  @Length(3, 99, {
    message: 'Name must be between 3 and 99 characters',
  })
  name: string;
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: 'Organization address',
    default: 'Kigali, Rwanda',
    type: String,
  })
  address: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    description: 'Organization thumbnail',
    default: 'logo.png',
    type: String,
  })
  logo: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: 'Organization category',
    default: 'Fashion',
    type: String,
  })
  industry: string;

  user: User;
}
