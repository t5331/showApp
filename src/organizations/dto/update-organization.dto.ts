import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
  Length,
} from 'class-validator';
import { User } from 'src/users/entities/user.entity';
import { CreateOrganizationDto } from './create-organization.dto';

export class UpdateOrganizationDto extends PartialType(CreateOrganizationDto) {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Organization name',
    default: 'Updated name',
    type: String,
  })
  @Length(4, 99, {
    message: 'Name must be between 4 and 99 characters',
  })
  name: string;
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: 'Organization address',
    default: 'Updated Kigali',
    type: String,
  })
  address: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    description: 'Organization Thumbnail',
    default: 'updated.jpg',
    type: String,
  })
  logo: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: 'Organization Thumbnail',
    default: 'Entertainment',
    type: String,
  })
  industry: string;
  user: User;
}
