import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Status } from '../common/enums/organization-status.enum';
import { UserRole } from '../common/enums/user-role.enum';
import { User } from '../users/entities/user.entity';
import { ILike, Repository } from 'typeorm';
import { CreateOrganizationDto } from './dto/create-organization.dto';
import { UpdateOrganizationDto } from './dto/update-organization.dto';
import { Organization } from './entities/organization.entity';

@Injectable()
export class OrganizationsService {
  constructor(
    @InjectRepository(Organization)
    private orgRepository: Repository<Organization>,
  ) {}
  /**
   * validate user
   * @param userId
   * @return promise<user>
   */
  private async validateOrgOwner(orgId: any, user: User): Promise<boolean> {
    if (user.role == UserRole.ORGANIZER) {
      const org = await this.orgRepository.findOne({
        where: {
          id: orgId,
          user,
        },
      });
      return org ? true : false;
    }
    return true;
  }

  /**
   * create new organization
   * @param requestBody
   * @return Object<organization>
   */
  async create(body: CreateOrganizationDto, user: User): Promise<Organization> {
    const newOrg = this.orgRepository.create({ ...body, user });
    return await this.orgRepository.save(newOrg);
  }

  /**
   * get all organizarons
   * @return promise<organization>
   */
  async findAll(status: Status): Promise<Organization[]> {
    if (status) {
      if (!Object.values(Status).includes(status))
        throw new BadRequestException('UNKNWON STATUS');
      return await this.orgRepository.find({
        where: {
          status,
        },
      });
    }
    return await this.orgRepository.find();
  }

  /**
   * get one organizarons
   * @param id<uuid>
   * @return promise<organization>
   */
  async findOne(id: string): Promise<Organization> {
    const org = await this.orgRepository.findOne(id);
    if (!org) throw new NotFoundException('Organization not found');

    return org;
  }

  /**
   * update organization
   * @param organization id <uuid>, user<User>, data<orgDTO>
   * @return promise<organization>
   */
  async update(
    id: string,
    orgData: UpdateOrganizationDto,
    user: User,
  ): Promise<Organization> {
    const org = await this.findOne(id);
    if (!org) throw new NotFoundException('Organization not found');

    const ownerShip = await this.validateOrgOwner(id, user);
    if (!ownerShip)
      throw new ForbiddenException('You can only update your organization');

    return await this.orgRepository.save({ ...org, ...orgData });
  }

  /**
   * Delete organization
   * @param organization id<uuid> user<User>
   * @return promise<organization>
   */
  async remove(id: string, user: User): Promise<[]> {
    const org = await this.findOne(id);
    if (!org || org.status == Status.DELETED)
      throw new NotFoundException('Organization not found!');

    const ownerShip = await this.validateOrgOwner(id, user);
    if (!ownerShip)
      throw new ForbiddenException('You can only Delete your organization');

    await this.orgRepository.save({ ...org, status: Status.DELETED });
    return [];
  }

  /**
   * Search organization
   * @param search keyword
   * @return promise<organization>
   */
  async searchOrganization(key: any): Promise<Organization[]> {
    try {
      return await this.orgRepository.find({
        where: [
          { name: ILike(`%${key.key}%`) },
          { industry: ILike(`%${key.key}%`) },
          { address: ILike(`%${key.key}%`) },
        ],
      });
    } catch (error) {
      throw new Error(error.message);
    }
  }

  /**
   * verify organization
   * @param organization id<uuid>
   * @return promise<organization>
   */
  async verifyOrganization(id): Promise<Organization> {
    const org = await this.findOne(id);
    if (org.status == Status.VERIFIED)
      throw new BadRequestException('Organization already verified!');
    if (org.status == Status.SUSPENDED || org.status == Status.DELETED)
      throw new BadRequestException(
        `can't verify a ${org.status} Organization`,
      );

    return await this.orgRepository.save({ ...org, status: Status.VERIFIED });
  }

  /**
   * events By Organization
   * @param organization id<uuid>
   * @return promise<organization>
   */
  async eventsByOrganization(id): Promise<Organization> {
    return await this.orgRepository.findOne(id, {
      relations: ['event'],
    });
  }

  /**
   * getUsers organization
   * @param organization user<User>
   * @return promise<organization>
   */
  async getUsersOrganization(user: User): Promise<Organization[]> {
    return await this.orgRepository.find({
      where: {
        user: user,
      },
    });
  }

  /**
   * suspend Organizationn
   * @param organization id<string>
   * @return promise<organization>
   */
  async suspendOrganization(id: string): Promise<Organization> {
    const org = await this.findOne(id);
    if (org.status != Status.VERIFIED)
      throw new BadRequestException(
        'Only verified organization can be suspended',
      );
    return await this.orgRepository.save({ ...org, status: Status.SUSPENDED });
  }

  /**
   * reactivate Organization
   * @param organization id<string>
   * @return promise<organization>
   */
  async reactivateOrganization(id: string): Promise<Organization> {
    const org = await this.findOne(id);
    if (org.status != Status.SUSPENDED)
      throw new BadRequestException('The organization is not suspended');
    return await this.orgRepository.save({ ...org, status: Status.VERIFIED });
  }
}
