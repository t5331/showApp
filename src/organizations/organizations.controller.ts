import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UsePipes,
  ValidationPipe,
  Query,
  ParseUUIDPipe,
  UseGuards,
} from '@nestjs/common';
import { OrganizationsService } from './organizations.service';
import { CreateOrganizationDto } from './dto/create-organization.dto';
import { UpdateOrganizationDto } from './dto/update-organization.dto';
import { QueryOrganizationDto } from './dto/query-organization.dto';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { UserRole } from 'src/common/enums/user-role.enum';
import { Roles } from 'src/auth/decorators/roles.decorator';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { GetUser } from 'src/auth/get-user.decorator';
import { User } from 'src/users/entities/user.entity';
import { Status } from 'src/common/enums/organization-status.enum';
import { ResponseInterface } from '../common/interfaces/response.interface';
import { Organization } from './entities/organization.entity';

@ApiTags('Organization')
@Controller('organizations')
export class OrganizationsController {
  constructor(private readonly organizationsService: OrganizationsService) {}

  @ApiTags('User journey')
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Organization created.' })
  @ApiBadRequestResponse({ description: 'Validation failed' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Roles(UserRole.ADMIN, UserRole.ORGANIZER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Post()
  async create(
    @Body() orgData: CreateOrganizationDto,
    @GetUser() user: User,
  ): Promise<ResponseInterface<Organization>> {
    return {
      message: 'Organization Created Successfully',
      data: await this.organizationsService.create(orgData, user),
    };
  }

  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Organization retrieved' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiQuery({ name: 'status', required: false })
  @Get()
  async findAll(
    @Query('status') status?: Status,
  ): Promise<ResponseInterface<Organization[]>> {
    return {
      message: 'Organization retrieved Successfully',
      data: await this.organizationsService.findAll(status),
    };
  }

  @ApiOkResponse({ description: 'Organization retrieved' })
  @ApiBadRequestResponse({ description: 'Validation failed' })
  @ApiNotFoundResponse({ description: 'Organization not found' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Get('single/:id')
  async findOne(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<ResponseInterface<Organization>> {
    return {
      message: 'Organization retrieved Successfully',
      data: await this.organizationsService.findOne(id),
    };
  }

  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Organization updated.' })
  @ApiForbiddenResponse({
    description: 'You can only Update your organization',
  })
  @ApiNotFoundResponse({ description: 'Organization not found' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Roles(UserRole.ADMIN, UserRole.ORGANIZER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateOrganizationDto: UpdateOrganizationDto,
    @GetUser() user: User,
  ): Promise<ResponseInterface<Organization>> {
    return {
      message: 'Organization updated Successfully',
      data: await this.organizationsService.update(
        id,
        updateOrganizationDto,
        user,
      ),
    };
  }

  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Organization deleted' })
  @ApiForbiddenResponse({
    description: 'You can only Delete your organization',
  })
  @ApiNotFoundResponse({ description: 'Organization not found' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Roles(UserRole.ADMIN, UserRole.ORGANIZER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Delete(':id')
  async remove(
    @Param('id', ParseUUIDPipe) id: string,
    @GetUser() user: User,
  ): Promise<ResponseInterface<[]>> {
    return {
      message: 'Organization deleted ',
      data: await this.organizationsService.remove(id, user),
    };
  }

  @ApiOkResponse({ description: 'Searched result' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @UsePipes(ValidationPipe)
  @ApiQuery({ name: 'key' })
  @Get('search')
  async search(
    @Query() key: QueryOrganizationDto,
  ): Promise<ResponseInterface<Organization[]>> {
    return {
      message: 'Search results',
      data: await this.organizationsService.searchOrganization(key),
    };
  }

  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Organization verified' })
  @ApiBadRequestResponse({ description: 'Organization already verified!' })
  @ApiNotFoundResponse({ description: 'Organization not found' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch('verify/:id')
  async verify(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<ResponseInterface<Organization>> {
    return {
      message: 'Organization Verified',
      data: await this.organizationsService.verifyOrganization(id),
    };
  }

  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Organization retrieved' })
  @ApiBadRequestResponse({ description: 'Validation error!' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Roles(UserRole.ADMIN, UserRole.ORGANIZER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Get(':id/events')
  async events(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<ResponseInterface<Organization>> {
    return {
      message: 'Organization events',
      data: await this.organizationsService.eventsByOrganization(id),
    };
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.ORGANIZER)
  @ApiOkResponse({ description: 'Organization retrieved' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Roles(UserRole.ADMIN, UserRole.ORGANIZER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Get('/user')
  async getOrganizationByUser(
    @GetUser() user: User,
  ): Promise<ResponseInterface<Organization[]>> {
    return {
      message: 'Organizations retrieved',
      data: await this.organizationsService.getUsersOrganization(user),
    };
  }

  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Organization suspended' })
  @ApiBadRequestResponse({
    description: 'Only verified organization can be suspended',
  })
  @ApiNotFoundResponse({ description: 'Organization not found' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch('suspend/:id')
  async suspendOrganization(
    @Param('id') id: string,
  ): Promise<ResponseInterface<Organization>> {
    return {
      message: 'Organization suspended.',
      data: await this.organizationsService.suspendOrganization(id),
    };
  }

  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Organization reactivated' })
  @ApiBadRequestResponse({ description: 'The organization is not suspended' })
  @ApiNotFoundResponse({ description: 'Organization not found' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch('reactivate/:id')
  async reactivateOrganization(
    @Param('id') id: string,
  ): Promise<ResponseInterface<Organization>> {
    return {
      message: 'Organization reactivated.',
      data: await this.organizationsService.reactivateOrganization(id),
    };
  }
}
