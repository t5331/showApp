import { define } from 'typeorm-seeding';
import { User } from '../../users/entities/user.entity';
import { UserRole } from '../../common/enums/user-role.enum';
import { hashPassword } from '../../common/helpers/hashPassword';
import * as dotenv from 'dotenv';

dotenv.config();

define(User, () => {
  const user = new User();
  user.names = process.env.ADMIN_NAMES;
  user.email = process.env.ADMIN_EMAIL;
  user.password = process.env.ADMIN_PASSWORD;
  user.phone = process.env.ADMIN_PHONE;
  user.role = UserRole.ADMIN;
  user.activated = true;
  return user;
});
