import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { RequestStatus } from '../../common/enums/request-status.enum';
export class SearchQueryDto {
  @IsString()
  @IsNotEmpty()
  @IsEnum(RequestStatus, {
    message: `status can only be ${RequestStatus.PENDING}, ${RequestStatus.APPROVED} or ${RequestStatus.REJECTED} `,
  })
  @ApiProperty({
    description: 'Request status ',
    default: RequestStatus.PENDING,
    type: [RequestStatus],
  })
  status: RequestStatus;
}
