import {
  Controller,
  Get,
  Post,
  Patch,
  Param,
  UseGuards,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { RequestsService } from './requests.service';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/get-user.decorator';
import { User } from 'src/users/entities/user.entity';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { SearchQueryDto } from './dto/searchQuery.dto';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/common/enums/user-role.enum';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { ResponseInterface } from '../common/interfaces/response.interface';
import { Request } from './entities/request.entity';

@ApiTags('Request')
@Controller('requests')
export class RequestsController {
  constructor(private readonly requestsService: RequestsService) {}

  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Requests retrieved successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @ApiForbiddenResponse({ description: 'Forbidden Resource' })
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Get()
  async findAll(): Promise<ResponseInterface<Request[]>> {
    return {
      message: 'Requests retrieved successfully',
      data: await this.requestsService.findAll(),
    };
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Get('search')
  @ApiOkResponse({ description: 'Requests retrieved successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @ApiForbiddenResponse({ description: 'Forbidden Resource' })
  @UsePipes(ValidationPipe)
  async searchRequest(
    @Query() query: SearchQueryDto,
  ): Promise<ResponseInterface<Request[]>> {
    return {
      message: 'Requests retrieved successfully',
      data: await this.requestsService.findRequests(query),
    };
  }

  @ApiTags('User journey')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Post()
  @ApiOkResponse({ description: 'Request sent successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @ApiForbiddenResponse({ description: 'Forbidden Resource' })
  async create(@GetUser() user: User): Promise<ResponseInterface<Request>> {
    const request = await this.requestsService.create(user.id);
    return { message: 'request created', data: request };
  }

  @ApiTags('User journey')
  @ApiBearerAuth()
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiOkResponse({ description: 'Request approved successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @ApiForbiddenResponse({ description: 'Forbidden Resource' })
  @Patch('/:id/approve')
  async approveRequest(
    @Param('id') id: string,
  ): Promise<ResponseInterface<Request>> {
    const request = await this.requestsService.approveRequest(id);
    return { message: 'request Approved', data: request };
  }

  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Request rejected successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @ApiForbiddenResponse({ description: 'Forbidden Resource' })
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch('/:id/reject')
  async rejectRequest(
    @Param('id') id: string,
  ): Promise<ResponseInterface<Request>> {
    const request = await this.requestsService.rejectRequest(id);
    return { message: 'request Approved', data: request };
  }

  @ApiBearerAuth()
  @ApiResponse({ status: 201, description: 'Request retrieved successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @ApiForbiddenResponse({ description: 'Forbidden Resource' })
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Get('/:id')
  async findOneRequest(
    @Param('id') id: string,
  ): Promise<ResponseInterface<Request>> {
    return {
      message: 'Request rejected successfully',
      data: await this.requestsService.findOne(id),
    };
  }
}
