import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from './entities/request.entity';
import { RequestStatus } from '../common/enums/request-status.enum';
import { UsersService } from '../users/users.service';
import { SearchQueryDto } from './dto/searchQuery.dto';

@Injectable()
export class RequestsService {
  constructor(
    @InjectRepository(Request) private requestRepo: Repository<Request>,
    private readonly userService: UsersService,
  ) {}

  async create(id: string): Promise<Request> {
    const request = await this.requestRepo.findOne({
      where: [
        { user: id, status: RequestStatus.PENDING },
        { user: id, status: RequestStatus.APPROVED },
      ],
      relations: ['user'],
    });

    if (request && request.status === RequestStatus.PENDING)
      throw new BadRequestException('You have a Pending request ');

    if (request && request.status === RequestStatus.APPROVED)
      throw new BadRequestException('you are already a manager');

    const user = await this.userService.findOne(id);

    return this.requestRepo.save({ user });
  }

  async findAll() {
    return this.requestRepo.find();
  }

  async approveRequest(id: string) {
    const request = await this.findOne(id);
    if (request.status != RequestStatus.PENDING)
      throw new BadRequestException('This request is no longer pending');

    const user = await this.userService.makeUserOrganizer(request.user.id);
    return this.requestRepo.save({
      ...request,
      status: RequestStatus.APPROVED,
    });
  }

  async findRequests(query: SearchQueryDto): Promise<Request[]> {
    return await this.requestRepo.find({
      where: { status: query.status },
    });
  }
  async rejectRequest(id: string) {
    const request = await this.findOne(id);
    if (request.status != RequestStatus.PENDING)
      throw new BadRequestException('This request is no longer pending');
    return this.requestRepo.save({
      ...request,
      status: RequestStatus.REJECTED,
    });
  }

  async findOne(id: string) {
    const request = await this.requestRepo.findOneOrFail(id, {
      relations: ['user'],
    });
    if (!request) throw new NotFoundException('request not found');
    return request;
  }
}
