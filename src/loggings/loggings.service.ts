import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateLoggingDto } from './dto/create-logging.dto';
import { Logging } from './entities/logging.entity';
import {StorageHelper} from '../common/helpers/storage.helper';

@Injectable()
export class LoggingsService {
  constructor(
    @InjectRepository(Logging)
     private logRepository: Repository<Logging>,
     private fileHelper:StorageHelper
  ) {}
  
  
  async create(createLoggingDto: CreateLoggingDto):Promise<Logging> {
    const newOrg = this.logRepository.create({ ...createLoggingDto});
    return await this.logRepository.save(newOrg);
  }

  async findAll() {
    return await this.logRepository.find();
  }
  
  @Cron(CronExpression.EVERY_WEEK)
  async createLogFile(){
    const logs = await this.findAll();
    const name=`${Date.now()}.log`;
    let logMessage='';
    if(logs.length>0){
      logs.forEach(item=>{
        logMessage+=`${item.title}  ${item.message} ${item.createdAt} \n`
      })
      if(this.fileHelper.createFile(name,logMessage)) await this.logRepository.clear()
    }
    
  }
}
