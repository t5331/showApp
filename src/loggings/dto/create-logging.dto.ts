import { IsNotEmpty, IsString } from "class-validator";

export class CreateLoggingDto {
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsString()
  message: string;

  @IsNotEmpty()
  @IsString()
  type: string;
}
