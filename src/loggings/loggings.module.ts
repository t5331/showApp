import { Module } from '@nestjs/common';
import { LoggingsService } from './loggings.service';
import { LoggingsController } from './loggings.controller';
import { Logging } from './entities/logging.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StorageHelper } from '../common/helpers/storage.helper';

@Module({
  imports:[TypeOrmModule.forFeature([Logging])],
  controllers: [LoggingsController],
  providers: [LoggingsService,StorageHelper],
  exports: [LoggingsService]
})
export class LoggingsModule {}
