import { Controller, Get, UseGuards } from '@nestjs/common';
import { LoggingsService } from './loggings.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOkResponse, ApiInternalServerErrorResponse, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { UserRole } from 'src/common/enums/user-role.enum';

@ApiTags('Logs')
@Controller('loggings')
export class LoggingsController {
  constructor(private readonly loggingsService: LoggingsService) {}

  @ApiBearerAuth()
  @ApiOkResponse({ description: 'logs retrieved'})
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Get()
  findAll() {
    return this.loggingsService.findAll();
  }

}
