import { Organization } from '../../organizations/entities/organization.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { EventStatus } from '../../common/enums/event-status.enum';
@Entity()
export class Event {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'text',
  })
  title: string;
  @ManyToOne(() => Organization,{onDelete:'CASCADE'})
  organization: Organization;

  @Column({
    nullable: false,
    type: 'text',
  })
  description: string;

  @Column({
    type: 'json',
    default:{'totalSeats':0,"price":0}
  })
  vip: JSON;

  @Column({
    type: 'json',
    default:{'totalSeats':0,"price":0}
  })
  vvip: JSON;

  @Column({
    type: 'json',
    default:{'totalSeats':0,"price":0}
  })
  standard: JSON;
  @Column({
    type: 'json',
    default:{'totalSeats':0,"price":0}
  })
  table: JSON;

  @Column({
    nullable: false,
    type: 'text',
  })
  venue: string;

  @Column({ type: 'enum', enum: EventStatus, default: EventStatus.ACTIVE })
  status: EventStatus;

  @Column({
    type: 'timestamptz',
  })
  startDate: Date;

  @Column({
    type: 'timestamptz',
  })
  endDate: Date;

  @CreateDateColumn({
    type: 'timestamptz',
    onUpdate: 'NOW()',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    onUpdate: 'NOW()',
  })
  updatedAt: Date;
}
