import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UsePipes,
  ValidationPipe,
  Query,
  UseGuards,
} from '@nestjs/common';
import { EventsService } from './events.service';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { SearchDto } from './dto/search.dto';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ResponseInterface } from '../common/interfaces/response.interface';
import { Event } from './entities/event.entity';
import { Roles } from '../auth/decorators/roles.decorator';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/guards/roles.guard';
import { UserRole } from '../common/enums/user-role.enum';
import { GetUser } from '../auth/get-user.decorator';
import { User } from '../users/entities/user.entity';
import { QueryDto } from './dto/queryDto.dto';
@ApiTags('Event')
@Controller('events')
export class EventsController {
  constructor(private readonly eventsService: EventsService) {}

  @ApiTags('User journey')
  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.ORGANIZER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Post()
  @UsePipes(ValidationPipe)
  @ApiCreatedResponse({ description: 'Event created successfully' })
  @ApiBadRequestResponse({ description: 'Organization not verified' })
  @ApiNotFoundResponse({ description: 'Organization does not exist' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  async create(
    @Body() createEventDto: CreateEventDto,
  ): Promise<ResponseInterface<Event>> {
    const event = await this.eventsService.create(createEventDto);
    return {
      message: 'Event created successfully',
      data: event,
    };
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Get('find')
  @ApiOkResponse({ description: 'Events retrieved successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  async findAll(@Query() query: QueryDto): Promise<ResponseInterface<Event[]>> {
    const events = await this.eventsService.findAll(query);
    return {
      message: 'Events retrieved successfully',
      data: events,
    };
  }

  @ApiTags('User journey')
  @Get()
  @ApiOkResponse({ description: 'Events retrieved successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  async findAllActive(): Promise<ResponseInterface<Event[]>> {
    const events = await this.eventsService.findAllActiveEvents();
    return {
      message: 'Events retrieved successfully',
      data: events,
    };
  }

  @Get(':id')
  @ApiOkResponse({ description: 'Event retrieved successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @ApiNotFoundResponse({ description: 'event does not exist' })
  async findOne(@Param('id') id: string): Promise<ResponseInterface<Event>> {
    const event = await this.eventsService.findOne(id);
    return {
      message: 'Event retrieved successfully',
      data: event,
    };
  }

  @Get('search')
  @UsePipes(ValidationPipe)
  @ApiOkResponse({ description: 'Events retrieved successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  async searchEvent(
    @Query() key: SearchDto,
  ): Promise<ResponseInterface<Event[]>> {
    const events = await this.eventsService.searchEvent(key);
    return {
      message: 'Events retrieved successfully',
      data: events,
    };
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.ORGANIZER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch('update/:id')
  @ApiOkResponse({ description: 'Event updated successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @ApiNotFoundResponse({ description: 'event does not exist' })
  @UsePipes(ValidationPipe)
  async update(
    @Param('id') id: string,
    @Body() updateEventDto: UpdateEventDto,
    @GetUser() user: User,
  ): Promise<ResponseInterface<Event>> {
    const event = await this.eventsService.update(id, updateEventDto, user);
    return {
      message: 'Event updated successfully',
      data: event,
    };
  }


  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.ORGANIZER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch('cancel/:id')
  @ApiOkResponse({ description: 'Event canceled successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @ApiNotFoundResponse({ description: 'event does not exist' })
  async cancelUpdate(
    @Param('id') id: string,
  ): Promise<ResponseInterface<Event>> {
    const event = await this.eventsService.cancelEvent(id);
    return {
      message: 'Event canceled successfully',
      data: event,
    };
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.ORGANIZER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Delete(':id')
  @ApiOkResponse({ description: 'Event deleted successfully' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @ApiNotFoundResponse({ description: 'event does not exist' })
  async remove(@Param('id') id: string): Promise<ResponseInterface<Event>> {
    const event = await this.eventsService.remove(id);
    return {
      message: 'Event deleted successfully',
      data: event,
    };
  }
}
