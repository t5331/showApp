import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, ILike, LessThan } from 'typeorm';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { Event } from './entities/event.entity';
import { OrganizationsService } from '../organizations/organizations.service';
import { Organization } from '../organizations/entities/organization.entity';
import { Cron, CronExpression } from '@nestjs/schedule';
import * as moment from 'moment';
import { EventStatus } from '../common/enums/event-status.enum';
import { QueryDto } from './dto/queryDto.dto';
import { User } from '../users/entities/user.entity';
import { UserRole } from '../common/enums/user-role.enum';
@Injectable()
export class EventsService {
  constructor(
    @InjectRepository(Event) private eventRepo: Repository<Event>,
    private readonly organizationsService: OrganizationsService,
  ) {}

  updateEvent(id: string, data: any): any {
    return this.eventRepo.update(id, data);
  }

  async create(createEventDto: CreateEventDto): Promise<Event> {
    const { organizationId } = createEventDto;
    const organization = await this.findOrganization(organizationId);

    if (!organization)
      throw new NotFoundException('Organization does not exist');

    if (!organization)
      throw new BadRequestException(
        'You can not create an event with unverified organization',
      );

    const newEvent = this.assignValueToEvent(createEventDto, new Event());

    try {
      return await this.eventRepo.save({ ...newEvent, organization });
    } catch (error) {
      throw new InternalServerErrorException(error.message);
    }
  }

  findAllActiveEvents(): Promise<Event[]> {
    try {
      return this.eventRepo.find({
        relations: ['organization'],
        where: {
          status: EventStatus.ACTIVE,
        },
      });
    } catch (error) {
      throw new InternalServerErrorException(error.message);
    }
  }

  findAll(query: QueryDto): Promise<Event[]> {
    try {
      return this.eventRepo.find({
        where: { status: query.status },
        relations: ['organization'],
      });
    } catch (error) {
      throw new InternalServerErrorException(error.message);
    }
  }

  async findOne(id: string): Promise<Event> {
    const event = await this.eventRepo.findOne(id, {
      relations: ['organization'],
    });

    if (!event) throw new NotFoundException('Event does not exist');
    return event;
  }

  async update(id: string, updateEventDto: UpdateEventDto, user: User) {
    let event = await this.findOne(id);
    if (!event) throw new NotFoundException('Event does not exist');

    if (
      user.role === UserRole.ORGANIZER &&
      user.id != event.organization.user.id
    )
      throw new ForbiddenException('Not allowed to do this action ');
    event = this.assignValueToEvent(updateEventDto, event);
    return this.eventRepo.save(event);
  }
 

  async cancelEvent(id: string) {
    const event = await this.findOne(id);
    if (!event) throw new NotFoundException('Event does not exist');
    return this.eventRepo.save({ ...event, status: EventStatus.CANCELED });
  }

  async remove(id: string) {
    const event = await this.findOne(id);
    if (!event) throw new NotFoundException('Event does not exist');
    return this.eventRepo.remove(event);
  }

  async findOrganization(id: string): Promise<Organization> {
    return await this.organizationsService.findOne(id);
  }

  async searchEvent(key: any): Promise<Event[]> {
    const keyword = key.key;
    return this.eventRepo.find({
      where: [
        { title: ILike(`%${keyword}%`) },
        { venue: ILike(`%${keyword}%`) },
      ],
    });
  }

  @Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT)
  async checkEndedEvents() {
    const events = await this.eventRepo.find({
      where: {
        status: EventStatus.ACTIVE,
        endDate: LessThan(new Date()),
      },
    });
    events.forEach((event) => {
      event.status = EventStatus.ENDED;
      this.eventRepo.save(event);
    });
  }

  //This fuctions takes in event dto and an event object and returns an updated version of the event object compared to the event dto
  assignValueToEvent(dto: any, event: Event): Event {
    const {
      title,
      description,
      venue,
      vip,
      vvip,
      table,
      standard,
      startDate,
      endDate,
    } = dto;

    if (title) event.title = title;

    if (description) event.description = description;

    if (venue) event.venue = venue;

    if (startDate) {
      if (!moment(new Date()).isSameOrBefore(startDate, 'day'))
        throw new BadRequestException(
          'You can not have an event with a starting date which is in past',
        );
      event.startDate = startDate;
    }

    if (endDate) {
      if (!moment(endDate).isSameOrAfter(startDate, 'day'))
        throw new BadRequestException(
          "You can not have an event that ends before it's starting date",
        );

      event.endDate = endDate;
    }

    // making a copy of the vip seats and update only the required property
    if (vip) event.vip = { ...event.vip, ...this.changeToJSON(vip) };

    // making a copy of the vvip seats and update only the required property
    if (vvip) event.vvip = { ...event.vvip, ...this.changeToJSON(vvip) };

    // making a copy of the standard seats and update only the required property
    if (standard)
      event.standard = { ...event.standard, ...this.changeToJSON(standard) };

    // making a copy of the table seats and update only the required property
    if (table) event.table = { ...event.table, ...this.changeToJSON(table) };

    // initializing the vip available seats by total seats
    if (event.vip && !event.vip['availableSeats'])
      event.vip['availableSeats'] = event.vip['totalSeats'];

    // initializing the vvip available seats by total seats
    if (event.vvip && !event.vvip['availableSeats'])
      event.vvip['availableSeats'] = event.vvip['totalSeats'];

    // initializing the standard available seats by total seats
    if (event.standard && !event.standard['availableSeats'])
      event.standard['availableSeats'] = event.standard['totalSeats'];

    // initializing the table available seats by total seats
    if (event.table && !event.table['availableSeats'])
      event.table['availableSeats'] = event.table['totalSeats'];

    return event;
  }

  //This fuctions takes an object and returns it's JSON format
  changeToJSON(object: any): JSON {
    return JSON.parse(JSON.stringify(object));
  }
}
