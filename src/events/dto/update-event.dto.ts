import { PartialType } from '@nestjs/mapped-types';
import { CreateEventDto } from './create-event.dto';
import {
  IsDate,
  IsInt,
  IsString,
  IsUUID,
  Matches,
  IsNotEmptyObject,
  IsDefined,
  IsObject,
  ValidateNested,
  IsNumber,
  IsOptional,
} from 'class-validator';

import { ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';

class TypePropertyDTO {
  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional({
    description: 'price',
    default: 5000,
    type: Number,
  })
  price: number;

  @IsOptional()
  @IsInt()
  @ApiPropertyOptional({
    description: 'total seats',
    default: 15000,
    type: Number,
  })
  totalSeats: number;

  @IsOptional()
  @IsInt()
  @ApiPropertyOptional({
    description: 'total seats',
    default: 15000,
    type: Number,
  })
  availableSeats: number;
}
export class UpdateEventDto extends PartialType(CreateEventDto) {
  @IsString()
  @IsOptional()
  @ApiPropertyOptional({
    description: 'Event title',
    default: 'Event one',
    type: String,
  })
  @Matches(/.?[^!@#$%*&^]/, {
    message: 'The event title can not be made with special characters only',
  })
  title: string;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({
    description: 'Event description',
    default: 'This event is happening now',
    type: String,
  })
  description: string;

  @IsString()
  @IsUUID()
  @IsOptional()
  @ApiPropertyOptional({
    description: 'Event organizer ID',
    default: 'a1822216-b222-4f3b-b1c8-319bd2f9f733',
    type: String,
  })
  organizationId: string;

  @IsDefined()
  @IsNotEmptyObject()
  @IsObject()
  @ValidateNested()
  @Type(() => TypePropertyDTO)
  @ApiPropertyOptional({
    description: 'Event vip seats details ',
    default: { price: 5000, totalSeats: 300 },
    type: [TypePropertyDTO],
  })
  vip: TypePropertyDTO;

  @IsDefined()
  @IsNotEmptyObject()
  @IsObject()
  @ValidateNested()
  @Type(() => TypePropertyDTO)
  @ApiPropertyOptional({
    description: 'Event vvip seats details ',
    default: { price: 5000, totalSeats: 300 },
    type: [TypePropertyDTO],
  })
  vvip: TypePropertyDTO;

  @IsDefined()
  @IsNotEmptyObject()
  @IsObject()
  @ValidateNested()
  @Type(() => TypePropertyDTO)
  @ApiPropertyOptional({
    description: 'Event ordinaly seats details ',
    default: { price: 5000, totalSeats: 300 },
    type: [TypePropertyDTO],
  })
  standard: TypePropertyDTO;

  @IsDefined()
  @IsNotEmptyObject()
  @IsObject()
  @ValidateNested()
  @Type(() => TypePropertyDTO)
  @ApiPropertyOptional({
    description: 'Event table seats details ',
    default: { price: 5000, totalSeats: 300 },
    type: [TypePropertyDTO],
  })
  table: TypePropertyDTO;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({
    description: 'Event venue ',
    default: 'Kigali Arena',
  })
  venue: string;

  @Type(() => Date)
  @IsDate()
  @IsOptional()
  @ApiPropertyOptional({
    description: 'Event start date',
    default: '12-31-2030',
  })
  startDate: Date;

  @Type(() => Date)
  @IsDate()
  @IsOptional()
  @ApiPropertyOptional({
    description: 'Event end date',
    default: '1-1-2031',
  })
  endDate: Date;
}
