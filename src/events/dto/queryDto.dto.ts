import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsString, Matches } from 'class-validator';
import { EventStatus } from 'src/common/enums/event-status.enum';

export class QueryDto {
    @IsString()
    @IsNotEmpty()
    @IsEnum(EventStatus, {
      message: `status can only be ${EventStatus.ACTIVE}, ${EventStatus.CANCELED} or ${EventStatus.ENDED} `,
    })
    @ApiProperty({
      description: 'Request status ',
      default: EventStatus.ACTIVE,
      type: [EventStatus],
    })
    status: EventStatus;
}
