import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Matches } from 'class-validator';

export class SearchDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'search key',
    default: 'Event one',
    type: String,
  })
  @Matches(/.?[^!@#$%*&^]/, {
    message: 'search key must not be made with spacial characters only',
  })
  key: string;
}
