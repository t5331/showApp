import {
  IsDate,
  IsInt,
  IsNotEmpty,
  IsString,
  IsUUID,
  Matches,
  IsNotEmptyObject,
  IsDefined,
  IsObject,
  ValidateNested,
  IsNumber,
  IsOptional,
} from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

class TypePropertyDTO {
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    description: 'price',
    default: 5000,
    type: Number,
  })
  price: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({
    description: 'total seats',
    default: 15000,
    type: Number,
  })
  totalSeats: number;

  @IsOptional()
  @IsInt()
  @ApiProperty({
    description: 'total seats',
    default: 15000,
    type: Number,
  })
  availableSeats: number;
}
export class CreateEventDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Event title',
    default: 'Event one',
    type: String,
  })
  @Matches(/.?[^!@#$%*&^]/, {
    message: 'The event title can not be made with special characters only',
  })
  title: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Event description',
    default: 'This event is happening now',
    type: String,
  })
  description: string;

  @IsString()
  @IsUUID()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Event organizer ID',
    default: 'a1822216-b222-4f3b-b1c8-319bd2f9f733',
    type: String,
  })
  organizationId: string;

  // @IsDefined()
  @IsOptional()
  @IsObject()
  @ValidateNested()
  @Type(() => TypePropertyDTO)
  @ApiProperty({
    description: 'Event vip seats details ',
    default: { price: 5000, totalSeats: 300 },
    type: [TypePropertyDTO],
  })
  vip: TypePropertyDTO;

  // @IsDefined()
  @IsOptional()
  @IsObject()
  @ValidateNested()
  @Type(() => TypePropertyDTO)
  @ApiProperty({
    description: 'Event vvip seats details ',
    default: { price: 5000, totalSeats: 300 },
    type: [TypePropertyDTO],
  })
  vvip: TypePropertyDTO;

  // @IsDefined()
  @IsOptional()
  @IsObject()
  @ValidateNested()
  @Type(() => TypePropertyDTO)
  @ApiProperty({
    description: 'Event ordinaly seats details ',
    default: { price: 5000, totalSeats: 300 },
    type: [TypePropertyDTO],
  })
  standard: TypePropertyDTO;

  // @IsDefined()
  @IsOptional()
  @IsObject()
  @ValidateNested()
  @Type(() => TypePropertyDTO)
  @ApiProperty({
    description: 'Event table seats details ',
    default: { price: 5000, totalSeats: 300 },
    type: [TypePropertyDTO],
  })
  table: TypePropertyDTO;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Event venue ',
    default: 'Kigali Arena',
  })
  venue: string;

  @Type(() => Date)
  @IsDate()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Event start date',
    default: '12-31-2030',
  })
  startDate: Date;

  @Type(() => Date)
  @IsDate()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Event end date',
    default: '1-1-2031',
  })
  endDate: Date;
}
