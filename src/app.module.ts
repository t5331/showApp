import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppConfigModule } from './common/config/app/config.module';
import { UsersModule } from './users/users.module';
import { EventsModule } from './events/events.module';
import { OrganizationsModule } from './organizations/organizations.module';
import { TicketsModule } from './tickets/tickets.module';
import { NotificationsModule } from './notifications/notifications.module';
import { AuthModule } from './auth/auth.module';
import { SendgridService } from './common/utils/sendgrid.service';
import DatabaseConfig from './common/config/database/config.orm';
import { ScheduleModule } from '@nestjs/schedule';
import { RequestsModule } from './requests/requests.module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { LoggingInterceptor } from './common/interceptors/logging.interceptor';
import { LoggingsModule } from './loggings/loggings.module';

@Module({
  imports: [
    AppConfigModule,
    TypeOrmModule.forRoot(DatabaseConfig),
    AuthModule,
    RequestsModule,
    OrganizationsModule,
    EventsModule,
    TicketsModule,
    UsersModule,
    NotificationsModule,
    ScheduleModule.forRoot(),
    LoggingsModule,
  ],

  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
    SendgridService,
  ],
})
export class AppModule {}
