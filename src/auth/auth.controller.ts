import {
  Controller,
  Post,
  Body,
  Get,
  UsePipes,
  ValidationPipe,
  Query,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { UserRole } from 'src/common/enums/user-role.enum';
import { ChangePasswordDto } from '../users/dto/change-password.dto';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { User } from '../users/entities/user.entity';
import { AuthService } from './auth.service';
import { Roles } from './decorators/roles.decorator';
import { ResetEmailDto } from './dto/reset-email-dto';
import { ResetPasswordDto } from './dto/reset-password-dto';
import { SignInDto } from './dto/signin-dto';
import { VerifyDto } from './dto/verify-dto';
import { GetUser } from './get-user.decorator';
import { RolesGuard } from './guards/roles.guard';

@ApiTags('Authentication')
@Controller('auth')
@UsePipes(ValidationPipe)
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  // LOGIN END-POINT
  @ApiTags('User journey')
  @ApiResponse({
    status: 201,
    description: 'Signed in successfully',
  })
  @ApiResponse({
    status: 401,
    description: `Please verify your account first`,
  })
  @ApiResponse({
    status: 401,
    description: `Invalid email and password`,
  })
  @Post('/login')
  async signin(@Body() signInDto: SignInDto): Promise<{ accessToken: string }> {
    return this.authService.login(signInDto);
  }

  // CREATE ADMIN END-POINT
  @ApiTags('User journey')
  @ApiResponse({
    status: 201,
    description: `Admin created successfully`,
  })
  @ApiResponse({
    status: 403,
    description: `Forbidden`,
  })
  @ApiResponse({
    status: 400,
    description: `Bad request`,
  })
  //  @ApiBearerAuth()
  //  @Roles(UserRole.ADMIN)
  //  @UseGuards(AuthGuard("jwt"), RolesGuard)
  @Post('/create-admin')
  async createAdmin(@Body() createAdminDto: CreateUserDto): Promise<any> {
    return await this.authService.createAdmin(createAdminDto);
  }

  // SIGNUP END-POINT
  @ApiTags('User journey')
  @ApiResponse({
    status: 201,
    description: `Signed up successfully, check your email to verify the account`,
  })
  @ApiResponse({
    status: 400,
    description: `Dad request`,
  })
  @ApiResponse({
    status: 409,
    description: `User with this email already exists`,
  })
  @Post('/attendee-register')
  async signup(@Body() createUserDto: CreateUserDto): Promise<any> {
    return this.authService.signup(createUserDto);
  }

  @ApiResponse({
    status: 201,
    description: `Signed up successfully, check your email to verify the account`,
  })
  @ApiResponse({
    status: 400,
    description: `Dad request`,
  })
  @ApiResponse({
    status: 409,
    description: `User with this email already exists`,
  })
  @Post('/organizer-register')
  async registerOrganizer(@Body() createUserDto: CreateUserDto): Promise<any> {
    return this.authService.organizerRegister(createUserDto);
  }

  // ACCOUNT VERIFICATION END-POINT
  @ApiResponse({
    status: 200,
    description: 'Account activate successfully',
  })
  @ApiResponse({
    status: 401,
    description: `The verification link provided is not valid`,
  })
  @Post('/verify')
  async verification(@Body() verifyDto: VerifyDto): Promise<any> {
    return await this.authService.verification(verifyDto);
  }

  // FORGOT PASSWORD END-POINT
  @ApiResponse({
    status: 200,
    description: 'Reset link is sent to this email: email provided',
  })
  @ApiResponse({
    status: 400,
    description: `Bad request`,
  })
  @ApiResponse({
    status: 404,
    description: `User with this email does no exist`,
  })
  @Post('/forgotpassword')
  async forgotpassword(@Body() resetEmail: ResetEmailDto) {
    return await this.authService.forgotpassword(resetEmail);
  }

  // RESET PASSWORD END-POINT
  @ApiResponse({
    status: 200,
    description: 'Password reset successfully',
  })
  @ApiResponse({
    status: 400,
    description: `Bad request`,
  })
  @ApiResponse({
    status: 401,
    description: `The reset link is expired or invalid due to modification`,
  })
  @Put('/resetpassword')
  async resetPassword(
    @Query() token,
    @Body() resetPasswordDto: ResetPasswordDto,
  ): Promise<any> {
    return await this.authService.resetPassword(token, resetPasswordDto);
  }
  // CHANGE PASSWORD END-POINT
  @ApiResponse({
    status: 200,
    description: `Password changed successfully`,
  })
  @ApiResponse({
    status: 500,
    description: `Failed to change password`,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Put('/changepassword')
  async changePassword(
    @Body() changePasswordDto: ChangePasswordDto,
    @GetUser() user: User,
  ) {
    return this.authService.changePassword(changePasswordDto, user);
  }
}
