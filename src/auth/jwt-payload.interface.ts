import { UserRole } from '../common/enums/user-role.enum';

export interface JwtPayload {
  id: string;
  role: UserRole;
}
