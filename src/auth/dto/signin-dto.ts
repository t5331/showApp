import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class SignInDto {
  @ApiProperty({
    description: 'Email',
    default: 'admin@showapp.com',
    type: 'string',
  })
  @IsNotEmpty()
  @IsEmail()
  email: string;
  @ApiProperty({
    description: 'Password',
    default: '@admin123',
    type: 'string',
  })
  @IsNotEmpty()
  password: string;
}
