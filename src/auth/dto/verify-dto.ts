import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class VerifyDto {
  @ApiProperty({
    description: 'code',
    default: '1234',
    type: 'number',
  })
  @IsNotEmpty()
  code: number;
}
