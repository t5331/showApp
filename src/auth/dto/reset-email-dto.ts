import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class ResetEmailDto {
  @ApiProperty({
    description: 'Email to receive reset lint on',
    default: 'faisalcyimana1998@gmail.com',
    type: 'string',
  })
  @IsNotEmpty()
  @IsEmail()
  email: string;
}
