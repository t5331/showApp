import {
  BadRequestException,
  ConflictException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { UserRepository } from '../users/users.repository';
import { SignInDto } from './dto/signin-dto';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './jwt-payload.interface';
import { emailTemplate } from '../common/helpers/email-template';
import { SendgridService } from '../common/utils/sendgrid.service';
import { ResetPasswordDto } from './dto/reset-password-dto';
import { resetEmailTmp } from '../common/helpers/reset-password-email-tmp';
import { ResetEmailDto } from './dto/reset-email-dto';
import { hashPassword } from '../common/helpers/hashPassword';
import { UserRole } from '../common/enums/user-role.enum';
import { ResponseInterface } from '../common/interfaces/response.interface';
import { ChangePasswordDto } from '../users/dto/change-password.dto';
import { User } from '../users/entities/user.entity';
import { codeGenerator } from 'src/common/helpers/codegenerator';
import { VerifyDto } from './dto/verify-dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  // SERVICE TO SIGNUP THE USER
  async signup(createUserDto: CreateUserDto): Promise<any> {
    // CHECKING IF THE USER EXISTS
    const isExistingUser = await this.userRepository.findOne({
      email: createUserDto.email,
    });
    // IF EXIST RETURN CONFLICT ERROR
    if (isExistingUser) {
      throw new ConflictException(
        'Account with this email already exists.',
      ).getResponse();
    }
    const verificationCode = codeGenerator();
    const userData = {
      names: createUserDto.names,
      email: createUserDto.email,
      password: await hashPassword(createUserDto.password), // HASHING THE PASSWORD
      phone: createUserDto.phone,
      verificationCode: verificationCode,
      role: UserRole.ORGANIZER,
    };
    // SAVING USER TO THE DATABASE
    const returnedUser = await this.userRepository.save(userData);
    // FORMATTING VERIFICATION EMAIL.
    const verificationMail = {
      to: returnedUser.email,
      subject: 'SHOWAPP ACCOUNT VERIFICATION',
      from: '',
      text: `Hello  ${returnedUser.names}, Now its time to verify your account`,
      html: emailTemplate(verificationCode),
    };
    await new SendgridService().send(verificationMail);
    const response: ResponseInterface<typeof returnedUser> = {
      message: 'Signed up successfully, check your email to verify the account',
      data: returnedUser,
    };
    return response;
  } // END OF SIGNUP SERVICE

  // SERVICE TO SIGNUP THE USER
  async organizerRegister(createUserDto: CreateUserDto): Promise<any> {
    // CHECKING IF THE USER EXISTS
    const isExistingUser = await this.userRepository.findOne({
      email: createUserDto.email,
    });
    // IF EXIST RETURN CONFLICT ERROR
    if (isExistingUser) {
      throw new ConflictException(
        'Account with this email already exists.',
      ).getResponse();
    }
    // FORMATTING THE USER DATA BEFORE PUSHING TO THE DATABASE
    const verificationCode = codeGenerator();
    const userData = {
      names: createUserDto.names,
      email: createUserDto.email,
      password: await hashPassword(createUserDto.password), // HASHING THE PASSWORD
      phone: createUserDto.phone,
      verificationCode: verificationCode,
      role: UserRole.ORGANIZER,
    };
    // SAVING USER TO THE DATABASE
    const returnedUser = await this.userRepository.save(userData);
    // FORMATTING VERIFICATION EMAIL.
    const verificationMail = {
      to: returnedUser.email,
      subject: 'SHOWAPP ACCOUNT VERIFICATION',
      from: '',
      text: `Hello  ${returnedUser.names}, Now its time to verify your account`,
      html: emailTemplate(verificationCode),
    };
    await new SendgridService().send(verificationMail);
    const response: ResponseInterface<typeof returnedUser> = {
      message: 'Signed up successfully, check your email to verify the account',
      data: returnedUser,
    };
    return response;
  } // END OF SIGNUP SERVICE

  // ACCOUNT VERIFICATION SERVICE
  async verification(verifyDto: VerifyDto): Promise<any> {
    const { code } = verifyDto;
    const user = await this.userRepository.findOne({ verificationCode: code });
    if (!user) {
      throw new UnauthorizedException('Invalid verification code');
    }
    if (user.activated) {
      throw new BadRequestException('Your account is activated already');
    }
    const userVerified = await this.userRepository.update(
      { id: user.id },
      { activated: true },
    );
    const response: ResponseInterface<typeof userVerified> = {
      message: 'Account verified successfully',
      data: userVerified,
    };
    return response;
  } // END OF ACCOUNT VERIFICATION SERVICE

  // LOGIN SERVICE
  async login(signInDto: SignInDto): Promise<any> {
    const { email, password } = signInDto;
    const user = await this.userRepository.findOne({ email });
    if (user && (await bcrypt.compare(password, user.password))) {
      // CHECK IF USER'S ACCOUNT IS ACTIVATED
      if (!user.activated) {
        throw new UnauthorizedException(
          'Please verify your account first',
        ).getResponse();
      }
      const payload: JwtPayload = {
        id: user.id,
        role: user.role,
      };
      const accessToke: string = await this.jwtService.sign(payload);

      const response: ResponseInterface<string> = {
        message: 'Signed in successfully',
        data: accessToke,
      };
      return response;
    } else {
      throw new UnauthorizedException(
        'Invalid email and password',
      ).getResponse();
    }
  } // LOGIN SERVICE

  // FORGOT PASSWORD SERVICE.
  async forgotpassword(resetEmail: ResetEmailDto): Promise<any> {
    const { email } = resetEmail;
    const user = await this.userRepository.findOne({ where: { email: email } });
    if (!user) {
      throw new NotFoundException(
        'User with this email does no exist',
      ).getResponse();
    }
    const payload = {
      id: user.id,
      role: user.role,
    };
    const token = await this.jwtService.sign(payload);
    const resetLink = `${process.env.APP_URL}/auth/resetpassword?token=${token}`;
    const resetPasswordEmail = {
      to: user.email,
      subject: 'SHOWAPP RESET PASSWORD',
      from: '',
      text: `Hello  ${user.names}`,
      html: resetEmailTmp(resetLink),
    };
    await new SendgridService().send(resetPasswordEmail);
    const response: ResponseInterface<[]> = {
      message: `Reset link is sent to this email: ${email}`,
      data: [],
    };
    return response;
  } // END OF FORGOT PASSWORD SERVICE

  async resetPassword(
    accessToken,
    resetPasswordDto: ResetPasswordDto,
  ): Promise<any> {
    const { token } = accessToken;
    const { newPassword } = resetPasswordDto;
    try {
      const decoded = await this.jwtService.verifyAsync(token);
      const hashedPassword = await hashPassword(newPassword);
      await this.userRepository.update(
        { id: decoded.id },
        { password: hashedPassword },
      );
      const response: ResponseInterface<[]> = {
        message: `Password reset successfully`,
        data: [],
      };
      return response;
    } catch (error) {
      throw new UnauthorizedException(
        'The reset link is expired or invalid due to modification',
      ).getResponse();
    }
  }
  // CHANGE PASSWORD SERVICE
  async changePassword(
    changePasswordDto: ChangePasswordDto,
    user: User,
  ): Promise<any> {
    const { password } = changePasswordDto;
    if (
      !this.userRepository.update(
        { id: user.id },
        { password: await hashPassword(password) },
      )
    ) {
      throw new InternalServerErrorException(
        'Failed to change password',
      ).getResponse();
    }
    const response: ResponseInterface<[]> = {
      message: `Password changed successfully`,
      data: [],
    };
    return response;
  }

  // CREATE SUPER ADMIN SERVICE
  async createAdmin(createAdminDto: CreateUserDto): Promise<any> {
    // CHECKING IF THE USER EXISTS
    const adminExist = await this.userRepository.findOne({
      email: createAdminDto.email,
    });
    // IF EXIST RETURN CONFLICT ERROR
    if (adminExist) {
      throw new ConflictException(
        'Admin with this email already exists.',
      ).getResponse();
    }
    const admin = {
      names: createAdminDto.names,
      email: createAdminDto.email,
      password: await hashPassword(createAdminDto.password),
      phone: createAdminDto.phone,
      role: UserRole.ADMIN,
      activated: true,
    };
    await this.userRepository.save(admin);
    return {
      message: 'Admin created successfully',
    };
  }
}
