import * as SendGrid from '@sendgrid/mail';
import * as dotenv from 'dotenv';

dotenv.config();

export class SendgridService {
  constructor() {
    SendGrid.setApiKey(process.env.SEND_GRID_KEY);
  }
  async send(mail: SendGrid.MailDataRequired) {
    mail.from = process.env.EMAIL;
    const transport = await SendGrid.send(mail);
    return transport;
  }
}
