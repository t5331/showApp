export const successResponse = (message: string, data: object) => {
  return {
    message,
    data,
  };
};
