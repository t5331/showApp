
import * as fs from 'fs'

export class StorageHelper {
 private  dir='./logs/'
 createFile(fileName:string,data:string):boolean {
     
  if(!fs.existsSync(this.dir)) fs.mkdirSync(this.dir);
  fs.writeFile(`${this.dir}${fileName}`, data,  function(err) {
      if (err) throw err;   
  });
  return true

}

}