export const dateCompare = (startDate, endDate) => {
  const date1 = new Date(startDate);
  const date2 = new Date(endDate);
  // Check if the dates are equal
  const same = date1.getTime() === date2.getTime();
  if (same) return 0;

  // Check if the startDate is greater than endDate
  if (date1 > date2) return -1;

  // Check if the endDate is less than startDate
  if (date2 > date1) return 1;
};
