import * as bcrypt from 'bcrypt';

export const hashPassword = async (plainPassword): Promise<string> => {
  const salt = await bcrypt.genSalt();
  const hashedPassword = await bcrypt.hash(plainPassword, salt);
  return hashedPassword;
};
