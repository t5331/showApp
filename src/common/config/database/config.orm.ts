import { DatabaseConfigService } from './config.service';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { User } from '../../../users/entities/user.entity';
import { Ticket } from '../../../tickets/entities/ticket.entity';
import { Organization } from '../../../organizations/entities/organization.entity';
import { Request } from '../../../requests/entities/request.entity';
import { Event } from '../../../events/entities/event.entity';
import * as dotenv from 'dotenv';
import { join } from 'path';
import { Logging } from '../../../loggings/entities/logging.entity';

dotenv.config();
const DatabaseConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: DatabaseConfigService.host,
  port: DatabaseConfigService.port,
  username: DatabaseConfigService.username,
  password: DatabaseConfigService.password,
  database: DatabaseConfigService.database,
  entities: [User, Organization, Event, Ticket, Request, Logging],
  migrations: [join(__dirname, '../../..') + '/migrations/*.ts'],
  cli: {
    migrationsDir: join(__dirname, '../../..') + '/migrations',
  },
  synchronize: process.env.APP_ENV !== 'production',
};

export default DatabaseConfig;
