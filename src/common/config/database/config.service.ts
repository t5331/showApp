import { databaseConfig } from './configuration';
import * as dotenv from 'dotenv';

dotenv.config();

export const DatabaseConfigService = databaseConfig[process.env.APP_ENV];
