import { DocumentBuilder, SwaggerCustomOptions } from '@nestjs/swagger';
export const config = new DocumentBuilder()
  .setTitle('ShowApp')
  .setDescription('The ShowApp API Documentation')
  .setVersion('1.0.0')
  .addTag('ShowApp')
  .addBearerAuth({
    description: 'Please enter token in following format: Bearer <JWT>',
    name: 'Authorization',
    bearerFormat: 'Bearer',
    scheme: 'Bearer',
    type: 'http',
    in: 'Header',
  })
  .build();
  export const customOptions: SwaggerCustomOptions = {
    swaggerOptions: {
      // persistAuthorization: true,
      tagsSorter: 'alpha',
      // operationsSorter: 'alpha',
    },
  };
  
