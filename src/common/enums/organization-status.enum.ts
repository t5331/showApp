export enum Status {
  VERIFIED = 'verified',
  UNVERIFIED = 'unverified',
  SUSPENDED = 'suspended',
  DELETED = 'deleted',
}
