export enum EventStatus {
    CANCELED = 'canceled',
    ACTIVE = 'active',
    ENDED = 'ended',
  }