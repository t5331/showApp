import { CallHandler, ExecutionContext, Injectable, Logger, NestInterceptor } from "@nestjs/common";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { LoggingsService } from "../../loggings/loggings.service";
import * as dotenv from 'dotenv';

dotenv.config();

@Injectable()
export class LoggingInterceptor implements NestInterceptor{
 constructor(
  private readonly loggingsService: LoggingsService      
 ){}

 intercept(context:ExecutionContext, next: CallHandler):Observable<any>{

  const req = context.switchToHttp().getRequest();
  const method = req.method;
  const url = req.url;
  const date = Date.now();
  const res = context.switchToHttp().getResponse()
  const status = res.statusCode
  const user = req.user;
  const message =`User: ${user?user.names:null} made a ${method} request to ${url} and got ${status} status code, and it took ${Date.now() - date}ms`;
  const log={title:context.getClass().name,message,type:status>299?'warning':status>399?'error':'sucess'};

  return next
         .handle()
         .pipe(
          tap(async()=> {
           if(process.env.APP_ENV!='development'){
                await this.loggingsService.create(log)
           }else{
                Logger.debug(message)
           }
          }),
         );
 }
}