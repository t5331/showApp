## Introdcution
ShowApp is a profiling & online ticketing system, where a user(attendee) will create his/her profile only once and use it in whichever events he/she wants to attend,and also the Organizer will have a platform to add his/her event and have stats or information of his previous events, since all data will be kept online, it's more like event managment and ticketing system.

## User
- Organizer(Manager)
- Attendee
- Adminstrator

## DB setup
![ERD](erd.png)
## Functionality

### ATTENDEE
- [X]  signup
- [X]  signin
- [X]  search event
- [X]  Buy event ticket
- [X]  Request Organizer access
- [X]  Request Ticket refund

### ORGANIZER
- [X]  Create an organization
- [X]  create an event
- [X]  View event stats
- [X]  Update organization
- [X]  Delete event

### ADMIN
- [X]  Comfirm access request

### System permissons
in showApp system the root user or admin, has maximum access or permission, attend except from signup and login he can search organization, attend event 

## Installation Guide
To install this project you have to clone into your local setup
### Requirements
- Node (obviously)
- DB(relational Database)
- NPM

### Pre-setup
Create a `.env` file then copy and paste data from `.env-example` into Your newly created `.env`,
make sure you fill all the information according to your setup
### Commands

#### install dependencies
```shell
npm i
```

#### Run Migration
```shell
npm run migration:generate
```

#### Run Application
```shell
npm run start:dev
```

#### Run Application
```shell
npm run start:dev
```

### Guide
#### API documentation

```
127.0.0.1:3000/api/v1/documentation
```




## Documentation
[docs](http://67.205.177.153:3000/api/v1/)
